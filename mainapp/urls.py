from django.urls import path
from .views import ProductsList, ProductsDetail, AboutProductsList, AboutProductsDetail


urlpatterns = [
	path('products/<int:pk>/', ProductsDetail.as_view()),
	path('products/', ProductsList.as_view()),
	path('aboutproducts/<int:pk>/', AboutProductsDetail.as_view()),
	path('aboutproducts/', AboutProductsList.as_view()),
]

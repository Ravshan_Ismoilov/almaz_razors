from django.contrib import admin
from .models import Products, AboutProducts
from parler.admin import TranslatableAdmin


@admin.register(Products)
class ProductsAdmin(TranslatableAdmin):
	list_display = ['title', 'created_by', 'created', 'img',]
	list_filter = ['created_by',]
	readonly_fields = ['created_by', 'created', 'updated',]
	search_fields = ['title', 'created_by', 'created',]

	def save_model(self, request, obj, form, change):
		obj.created_by = request.user
		super().save_model(request, obj, form, change)

@admin.register(AboutProducts)
class AboutProductsAdmin(TranslatableAdmin):
	list_display = ['title', 'created_by', 'created', 'img',]
	list_filter = ['created_by',]
	readonly_fields = ['created_by', 'created', 'updated',]
	search_fields = ['title', 'created_by', 'created',]

	def save_model(self, request, obj, form, change):
		obj.created_by = request.user
		super().save_model(request, obj, form, change)
from django.db import models
from django.urls import reverse
from django.utils.html import mark_safe
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _
from parler.models import TranslatableModel, TranslatedFields
from django.utils import timezone
import datetime

User = get_user_model()

timeDurationRegex = RegexValidator(regex = r"^\d{1,3}$")


STATUS_CHOICES = (
	('active', _('Active')),
	('qarzdor', _('Qarzdor')),
	('arxiv', _('Arxiv')),
	('delete', _('Deleted'))
	)

# class ActiveManager(models.Manager):
# 	def get_queryset(self):
# 		return super().get_queryset().filter(status='active')

class Products(TranslatableModel):
	translations = TranslatedFields(
		title = models.CharField(_("Title"),max_length=255,),
		description = models.TextField(_("Description"), blank=True, null=True)
	)
	image = models.ImageField(upload_to="mahsulot/slayder/%Y/%m/%d/", verbose_name="Mahsulot rasmi")
	# status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active')
	created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="related_user_products", verbose_name="Xodim")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
	updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
	# objects = models.Manager()
	# active = ActiveManager()

	def img(self):
		return mark_safe('<img src="/media/%s" width="50" height="50"/>' % (self.image))
	img.short_description = 'Rasmi'

	def __str__(self):
		return self.title

	class Meta:
		# ordering = ('-created',)
		verbose_name = _("Mahsulot")
		verbose_name_plural = _("Mahsulot")

class AboutProducts(TranslatableModel):
	translations = TranslatedFields(
		title = models.CharField(_("Title"), max_length=255,),
		description = models.TextField(_("Description"), blank=True, null=True),
	)
	image = models.ImageField(upload_to="mahsulot/haqida/%Y/%m/%d/", verbose_name="Mahsulot rasmi")
	# status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active')
	created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="related_user_aboutproducts", verbose_name="Xodim")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Qo'shilgan vaqti")
	updated = models.DateTimeField(auto_now=True, verbose_name="O'zgartirilgan vaqti")
	# objects = models.Manager()
	# active = ActiveManager()

	def img(self):
		return mark_safe('<img src="/media/%s" width="50" height="50"/>' % (self.image))
	img.short_description = 'Rasmi'

	def __str__(self):
		return self.title

	class Meta:
		# ordering = ('-created',)
		verbose_name = _("Batafsil")
		verbose_name_plural = _("Batafsil")
from django.shortcuts import render
from rest_framework import generics
from .models import Products, AboutProducts
from .serializers import AboutProductsSerializer, ProductsSerializer


class ProductsList(generics.ListCreateAPIView):
	queryset = Products.objects.all()
	serializer_class = ProductsSerializer

class ProductsDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Products.objects.all()
	serializer_class = ProductsSerializer



class AboutProductsList(generics.ListCreateAPIView):
	queryset = AboutProducts.objects.all()
	serializer_class = AboutProductsSerializer

class AboutProductsDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = AboutProducts.objects.all()
	serializer_class = AboutProductsSerializer
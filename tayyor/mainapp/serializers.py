from rest_framework import serializers
from parler_rest.serializers import TranslatableModelSerializer, TranslatedFieldsField
from .models import Products, AboutProducts


class ProductsSerializer(TranslatableModelSerializer):
	translations = TranslatedFieldsField(shared_model=Products)

	class Meta:
		model = Products
		fields = ('id', 'translations', 'image', 'created_by', 'created', 'updated')

class AboutProductsSerializer(TranslatableModelSerializer):
	translations = TranslatedFieldsField(shared_model=AboutProducts)

	class Meta:
		model = AboutProducts
		fields = ('id', 'translations', 'image', 'created_by', 'created', 'updated')